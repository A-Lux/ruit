<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ $page->getMeta()->description }}">
    <meta name="keyword" content="{{ $page->getMeta()->keyword }}">
    <meta name="base" content="{{ url('/') }}">
    <title>{{ $page->getMeta()->title }}</title>

    <link rel="shortcut icon" href="{{ Voyager::image(setting('site.logotype')) }}" type="image/x-icon">
    <link rel="icon" href="{{ Voyager::image(setting('site.logotype')) }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{asset('css/fixes.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body>
