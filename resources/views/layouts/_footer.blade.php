<footer class="footer" style="margin-top:50px">
    @if(Request::segment(1) != 'contacts')
        <div class="containerR">
            <div class="h1">Контактная информация</div>
        </div>
    @endif

    <div class="map_block">
        <div class="map_iframe">
            <iframe src="{{ $contact->map_url }}" width="100%" height="575" frameborder="0" style="border:0;"
                    allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        <div class="footer_info">
            <div class="footer_info_title">{{ $contact->title }}</div>
            <div class="contact_f_item">
                <div class="icon">
                    <img src="{{ asset('images/SVG/map.svg') }}" alt="">
                </div>
                <div class="detail">
                    {{ $contact->address }}
                </div>
            </div>
            <div class="contact_f_item">
                <div class="icon">
                    <img src="{{ asset('images/SVG/mail.svg') }}" alt="">
                </div>
                <div class="detail">
                    <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                </div>
            </div>
            <div class="contact_f_item">
                <div class="icon">
                    <img src="{{ asset('images/SVG/phone.svg') }}" alt="">
                </div>
                <div class="detail">
                    <a href="tel:{{ $contact->phone_almaty }}">{{ $contact->phone_almaty }}</a>
                </div>
            </div>
            <div class="contact_f_item">
                <div class="icon">
                    <img src="{{ asset('images/SVG/call.svg') }}" alt="">
                </div>
                <div class="detail">
                    <a href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a>
                </div>
            </div>
            <a href="{{ route('contacts') }}#feedbackForm" class="yellow_btn">
                Обратная связь
            </a>
        </div>
    </div>

    @if(Request::segment(1) == 'contacts')
        <div class="contacts_form">
            <div class="containerR">
                <div class="contacts_form_left">
                    <div class="cont_title">Оставьте свое сообщение <br>и контакты, мы с вами <br>свяжемся</div>
                    <div class="cont_sub_title">+ дадим полезную информацию</div>
                    <div class="cont_row">
                        <img src="images/cont_tel.png" alt="">
                        <div class="text">{{ setting('site.contact') }}</div>
                    </div>
                </div>
                <div class="contacts_form_right">
                    <form id="feedbackForm">
                        <div class="contacts_form_row">
                            <label class="requared">
                                <input name="name" type="text" class="input_f " placeholder="Имя">
                                <span id="name_error"></span>
                            </label>
                            <label>
                                <input name="phone" type="text" class="input_f "
                                       placeholder="Телефон, например: +7705505500">
                                <span id="phone_error"></span>
                            </label>
                            <label class="requared">
                                <input name="email" type="text" class="input_f requared" placeholder="Эл. почта">
                                <span id="email_error"></span>
                            </label>
                        </div>
                        <textarea class="input_f" name="message"
                                  placeholder="Расскажите коротко о проекте, прикрепите ТЗ (если есть) или задайте свой вопрос"></textarea>
                        <span id="message_error"></span>

                        <div class="contacts_form_row_sbm">
                            <button type="button" class="sbm_btn">Отправить заявку</button>
                            <div class="polit">
                                        <span class="checkb">
                                            <input type="checkbox" id="checkb">
                                            <label for="checkb"></label>
                                        </span>
                                <span class="text">Даю согласие на обработку
                                    <a href="{{ json_decode(setting('site.personal_data')) != null ?
                                        Voyager::image(json_decode(setting('site.personal_data'))[0]->download_link) : "" }}"
                                       target="_blank">
                                        персональных данных
                                    </a>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

    <div class="containerR under_line">
        <div class="copyr">{{ setting('site.copyright') }}</div>
        <div class="social">
            @foreach($socialNetwork as $v)
                @if($v->type == 1)
                    <a href="{{ $v->url }}" class="inst" target="_blank">
                    <span>
                        <img src="{{ asset('images/footer_insta.png') }}" alt="">
                    </span>
                        {{ $v->name }}
                    </a>
                @else
                    <a href="{{ $v->url }}" class="fb" target="_blank">
                        <span>
                            <img src="{{ asset('images/footer_fb.png') }}" alt="">
                        </span>
                        {{ $v->name }}
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('/js/jquery.maskedinput.min.js') }}"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('/slick/slick.min.js') }}"></script>
<script src="{{ asset('/js/script.js') }}"></script>
<script src="{{ asset('/js/ajax.js') }}"></script>

</body>
</html>



