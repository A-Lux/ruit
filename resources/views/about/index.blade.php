@extends('layouts.app')
@section('content')

    <div class="top_block">

        @include('/partials/menu')
        @include('/partials/breadcrumbs')

        <div class="main_catalog">
            <div class="containerR">
                <h1>
                    {{ $page->getPage()->getFirstTitle() }}
                </h1>
                <div class="about_top_row">
                    <div class="about_left">
                        {!! $banner->left_content !!}
                    </div>
                    <div class="about_right">
                        <div class="text">
                            {!! $banner->right_content !!}
                        </div>
                        <a href="{{ route('contacts') }}#feedbackForm" class="yellow_btn">Обратная связь</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77717533249&text=Здравствуйте!%20Пишу%20с%20города"</a>
        <div class="circlephone" style="transform-origin: center;">
         <div class="circle-fill" style="transform-origin: center;">
      </div>
    </div>

    <div class="about_white">
        <div class="containerR">
            <div class="white_row">
                @foreach($advantages as $advantage)
                    <div class="item">{{ $advantage->name }}</div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="about_midle">
        <div class="containerR">

            <div class="midle_left">
                <div class="about_title">{!! $about->title !!}</div>
                <div class="about_sub">
                    {!! $about->rectangle_content !!}
                </div>
                <div class="about_text">
                    {!! $about->bottom_content !!}
                </div>
            </div>
            <div class="midle_img_pos">
                <img src="{{ asset('images/city.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77717533249&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5,%20%D0%BC%D0%B5%D0%BD%D1%8F%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0%20%D0%B2%D0%B5%D0%B1%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0."</a>
        <div class="circlephone" style="transform-origin: center;">
         <div class="circle-fill" style="transform-origin: center;">
      </div>
    <style>
        .containerR .h1{
            display: none;
        }
    </style>
@endsection
