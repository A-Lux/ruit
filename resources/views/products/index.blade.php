@extends('layouts.app')
@section('content')

    @include('/partials/menu')
    @include('/partials/breadcrumbs')
    <div class="main_catalog">
        <div class="containerR">
            <div class="top_row">
                <h1> {{ $page->getPage()->getFirstTitle() }}</h1>
                @if(json_decode(setting('site.catalogs')) != null)
                    <span>
                        <a href="{{ Voyager::image(json_decode(setting('site.catalogs'))[0]->download_link) }}" class="download_f" target="_blank">
                            Скачать полный каталог  </a>>
                    </span>
                @endif
            </div>
            <div class="catalog_row">
                @foreach($products as $product)
                    <div class="catalog_item">
                        <a href="{{ route('product', ['url' => $product->url]) }}" class="catalog_item_ins">
                                <span class="catalog_img">
                                    <img src="{{ Voyager::image($product->image) }}" alt="">
                                </span>
                            <span class="catalog_name">{{ $product->name }}</span>
                            <span class="catalog_detail yellow_btn">Подробнее</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77717533249&text=Здравствуйте!%20Пишу%20с%20города"</a>
        <div class="circlephone" style="transform-origin: center;">
         <div class="circle-fill" style="transform-origin: center;">
      </div>
@endsection
