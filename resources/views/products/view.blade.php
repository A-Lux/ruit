@extends('layouts.app')
@section('content')

    @include('/partials/menu')
    @include('/partials/breadcrumbs')
    <div class="main_catalog">
        <div class="containerR">
            <div class="top_row">
                <h1>{{ $product->name }}</h1>
            </div>
        </div>
    </div>
    <div class="single">
        <div class="containerR">

            <div class="single_left">
                <div class="photo_place">
                    <img src="{{ Voyager::image($product->image) }}" alt="">
                </div>
                <div class="links_place">
                    @if(json_decode($product->file) != null)
                        <a href="{{ Voyager::image(json_decode($product->file)[0]->download_link) }}" class="yellow_btn link_left" target="_blank">
                            Скачать каталог
                        </a>
                    @endif
                    <a href="{{ route('contacts') }}#feedbackForm" class=" yellow_btn link_right">Обратная связь</a>
                </div>
            </div>

            <div class="single_right">
                <div class="single_det">Технические характеристики</div>
                <div class="single_ul">
                   {!! $product->specification !!}
                </div>
                <div class="single_det">Габаритные и установочные размеры</div>
                <div class="block_for_scroll">
                    {!! $product->dimensions !!}
                </div>
            </div>
        </div>
    </div>

@endsection
