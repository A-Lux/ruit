@extends('layouts.app')
@section('content')

    @include('/partials/menu')
    @include('/partials/breadcrumbs')

    <div class="main_catalog">
        <div class="containerR">
            <div class="top_row">
                <h1>{{ $page->getPage()->getFirstTitle() }}</h1>
                <div class="navig_slider">
                    <div class="prev">
                        <img src="{{ asset('images/SVG/arrow laft.svg') }}" alt="">
                    </div>
                    <div class="next">
                        <img src="{{ asset('images/SVG/arrow rigth.svg') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="slider_sertificat popupe">
                @foreach($certificates as $certificate)
                    <a href="{{ Voyager::image($certificate->image) }}">
                        <img class="sert_item" src="{{ Voyager::image($certificate->image) }}" alt="">
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77717533249&text=Здравствуйте!%20Пишу%20с%20города"</a>
        <div class="circlephone" style="transform-origin: center;">
         <div class="circle-fill" style="transform-origin: center;">
      </div>

@endsection
