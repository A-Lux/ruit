@extends('layouts.app')
@section('content')

    <div class="top_block" style="background-image: url({{ asset('images/index_bg.jpg') }});">
        @include('/partials/menu')
        <div class="main_catalog">
            <div class="containerR">
                <div class="index_top">
                    <div class="color_index">{{ $banner->subtitle }}</div>
                    <div class="index_title">{!! $banner->title !!}</div>
                    <div class="index_sub">
                        {!! $banner->description !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="blue_opacity blue_opacity_left"></div>
        <div class="blue_opacity blue_opacity_right"></div>
    </div>

    <div class="index_about">
        <div class="containerR">
            <div class="title">
                {{ $page->getPage()->getFirstTitle() }}
            </div>

            {!! $about->content !!}

            <a href="{{ route('about') }}" class="yellow_btn">Подробнее</a>
            <div class="index_about_absolute">
                <img src="{{ asset('images/img_about.jpg') }}" alt="">
            </div>
            <div class="blue_opacity blue_opacity_about"></div>
        </div>
    </div>

    <div class="our_best">
        <div class="containerR">
            <div class="index_best_center">
                <div class="index_best_title"> {{ $page->getPage()->getSecondTitle() }}</div>
                <div class="index_best_row">
                    @foreach($advantages as $v)
                        <div class="index_best_item">
                            <div class="tit">0{{ $loop->iteration }}</div>
                            <div class="descr">{!! $v->content !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="Slider_sertif">
        <div class="containerR">
            <div class="img_sert_absolute">
                <img src="{{ asset('images/img_sertif.jpg') }}" alt="">
            </div>
            <div class="top_row">
                <div class="h1"> {{ $page->getPage()->getThirdTitle() }}</div>
                <div class="navig_slider">
                    <div class="prev">
                        <img src="{{ asset('images/SVG/arrow laft.svg') }}" alt="">
                    </div>
                    <div class="next">
                        <img src="{{ asset('images/SVG/arrow rigth.svg') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="slider_sertificat_index popupe">
                @foreach($certificates as $v)
                    <a href="{{ Voyager::image($v->image) }}">
                        <img class="sert_item" src="{{ Voyager::image($v->image) }}" alt="">
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+77717533249&text=Здравствуйте!%20Пишу%20с%20города"</a>
        <div class="circlephone" style="transform-origin: center;">
         <div class="circle-fill" style="transform-origin: center;">
      </div>
@endsection
