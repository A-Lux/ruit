@component('mail::message')
# Новая заявка

Поступила новая заявка на сайте Ruit
@component('mail::button', ['url' => 'https://ruit.co/admin/requests'])
    Просмотреть заявки
@endcomponent

{{--Thanks,<br>--}}
{{ config('app.name') }}
@endcomponent
