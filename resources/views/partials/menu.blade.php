<header class="header">
    <div class="containerR">
        <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="logo">
            @if(Request::segment(1) != '')
                <a href="{{ route('home') }}">
                    <img src="{{ Voyager::image(setting('site.logotype')) }}" alt="">
                </a>
            @else
                <img src="{{ Voyager::image(setting('site.logotype')) }}" alt="">
            @endif
        </div>
        <div class="navig">
            <ul>
                @foreach (menu('site', '_json') as $menuItem)
                    @if(\Illuminate\Support\Facades\Request::segment(1) == $menuItem->url))
                        <li class="active">
                            <a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="header_right">
            <a href="{{ route('contacts') }}#feedbackForm" class="callback yellow_btn">Обратная связь</a>
        </div>
    </div>
</header>
