<div class="breadcrumbs">
    <div class="containerR">
        <div class="breadcr">
            <ul>
                <li>
                    <a href="{{ route('home') }}">Главная</a>
                </li>
            @if(isset($g_product) && Request::segment(1) == 'catalogs')
                <li>
                    <span>/</span>
                    <a href="{{ route('products') }}">
                        Каталог
                    </a>
                </li>
                <li>
                    <span>/</span>
                    {{ $g_product->name }}
                </li>
            @else
                <li>
                    <span>/</span>
                    {{ $page->getPage()->getFirstTitle() }}
                </li>
            @endif
            </ul>
        </div>
    </div>
</div>
