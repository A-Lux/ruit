<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutBanner extends Model
{
    public static function getContent(){
        return self::select('left_content', 'right_content')->first();
    }
}
