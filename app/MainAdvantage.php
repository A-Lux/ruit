<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainAdvantage extends Model
{
    public static function getAll(){
        return self::select('id', 'content')
            ->get();
    }
}
