<?php


namespace App\Traits;


use Illuminate\Http\Request;

trait VoyagerSinglePage
{
    public function index(Request $request){
        return parent::show($request, 1);
    }
}
