<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SocialNetwork extends Model
{
    public static function getAll(){
        return self::select('id', 'type', 'name', 'url')->get();
    }
}
