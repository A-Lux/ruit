<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Page extends Model
{

    public function getTitle(){
        return $this->hasMany('App\Title');
    }

    public function getFirstTitle(){
        return $this->getTitle() ? $this->getTitle()->first()->text : "";
    }

    public function getSecondTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(1)->first() ?
            $this->getTitle()->skip(1)->first()->text : "") : "";
    }

    public function getThirdTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(2)->first() ?
            $this->getTitle()->skip(2)->first()->text : "") : "";
    }

    public function getForthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(3)->first() ?
            $this->getTitle()->skip(3)->first()->text : "") : "";
    }

    public function getFifthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(4)->first() ?
            $this->getTitle()->skip(4)->first()->text : "") : "";
    }


    public static function getByUrl($url){
        return self::where('url', $url)->first();
    }


}
