<?php

namespace App\Providers;

use App\Page\Page;
use App\Page as PageModel;
use App\Product as ProductModel;
use App\Meta\MetaTag;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class MetaTagsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    protected $page;

    public function register()
    {
        $this->app->singleton(MetaTag::class, function ($app) {
            return new MetaTag(config('app.meta'));
        });

        $this->app->singleton(Page::class, function ($app) {
            return new Page();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(MetaTag $meta, Page $page)
    {
        $pageInstance = PageModel::getByUrl(Request::segment(1));
        $projectInstance = ProductModel::getByUrl(Request::segment(2));

       if($projectInstance && Request::segment(1) == "catalogs"){
            $page->setPage($projectInstance);
            if($projectInstance->meta_title) {
                $meta->setTitle($projectInstance->meta_title);
                $meta->setDescription($projectInstance->meta_description);
                $meta->setKeyword($projectInstance->meta_keyword);
            }
        }else if($pageInstance) {
            $page->setPage($pageInstance);
            if($pageInstance->meta_title) {
                $meta->setTitle($pageInstance->meta_title);
                $meta->setDescription($pageInstance->meta_description);
                $meta->setKeyword($pageInstance->meta_keyword);
            }
        }

        $page->setMeta($meta);
    }
}
