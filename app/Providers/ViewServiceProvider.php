<?php

namespace App\Providers;

use App\Page\Page;
use App\Product;
use App\Contact;
use App\SocialNetwork;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Product $product, Contact $contact, SocialNetwork $socialNetwork)
    {

        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });

        View::composer('*', function($view) use($product) {
            $product = $product::getByUrl( Request::segment(2));
            if($product){
                $view->with(['g_product' => $product]);
            }
        });


        View::composer('*', function($view) use($contact) {
            $contact = $contact::getContent();
            $view->with(['contact' => $contact]);
        });


        View::composer('*', function($view) use($socialNetwork) {
            $socialNetwork = $socialNetwork::getAll();
            $view->with(['socialNetwork' => $socialNetwork]);
        });


    }
}
