<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    public static function getAll(){
        return self::select('id', 'name', 'image', 'url', 'sort')
            ->orderBy('sort', 'ASC')
            ->get();
    }

    public static function getByUrl($url){
        return self::select('id', 'name', 'image', 'specification', 'dimensions', 'file', 'meta_title',
            'meta_description', 'meta_keyword', 'url')
            ->where('url', $url)
            ->first();
    }
}
