<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Certificate extends Model
{
    const on_main = 1;
    const not_on_main = 0;

    public static function getMain(){
        return self::select('id', 'image', 'on_main', 'sort')
            ->where('on_main', self::on_main)
            ->orderBy('sort', 'ASC')
            ->get();
    }


    public static function getAll(){
        return self::select('id', 'image', 'sort')
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
