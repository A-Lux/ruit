<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 22:55
 */

namespace App\Mail;

use App\Request as Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Feedback
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Feedback $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.feedback')->subject("Пользователь хочет связаться с вами");
    }
}
