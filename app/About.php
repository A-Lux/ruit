<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class About extends Model
{
    public static function getContent(){
        return self::select('title', 'rectangle_content', 'bottom_content', 'image')->first();
    }
}
