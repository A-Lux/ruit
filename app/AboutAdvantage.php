<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutAdvantage extends Model
{
    public static function getAll(){
        return self::select('id', 'name', 'sort')
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
