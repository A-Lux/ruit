<?php


namespace App\Http\Controllers\Admin;


use App\Traits\VoyagerSinglePage;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutBannersController extends VoyagerBaseController
{
    use VoyagerSinglePage;
}
