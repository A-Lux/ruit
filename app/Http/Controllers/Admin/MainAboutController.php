<?php


namespace App\Http\Controllers\Admin;


use App\Traits\VoyagerSinglePage;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MainAboutController extends VoyagerBaseController
{
    use VoyagerSinglePage;
}
