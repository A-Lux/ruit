<?php

namespace App\Http\Controllers;

use App\Mail\NewRequest;
use App\Mail\RequestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Request as Feedback;
use App\Mail\FeedbackRequest;

class RequestController extends Controller
{
    const success = 1;
    const fail = 0;

    public function feedback(Request $request) {

        $rules = [
            'name'          => 'required|max:255',
            'email'         => 'required|max:255|email',
        ];
//        https://ruit.a-lux.dev/storage/main-abouts/August2020/logo1.png

        if ($request['phone'] != null){
            $rules['phone'] = 'required|max:255|regex:/(\+77)[0-9]{9}/';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response([
                'status' => self::fail,
                'errors' => $errors->toArray()
            ], 200);
        }else{
            $model = Feedback::create($request->all());
            Mail::to('marketing@siegeria.com')->send(new NewRequest());

            return response([
                'status'    => self::success,
                'title'     => 'Заявка успешно отправлена!',
                'content'   => 'В ближайшее время мы свяжемся с вами',
            ], 200);
        }
    }


}
