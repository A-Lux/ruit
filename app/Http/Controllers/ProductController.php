<?php


namespace App\Http\Controllers;


use App\Product;

class ProductController extends Controller
{
    public function index(){

        $products = Product::getAll();

        return view('products.index', compact('products'));
    }

    public function view($url){

        $product = Product::getByUrl($url);

        return view('products.view', compact('product'));
    }
}
