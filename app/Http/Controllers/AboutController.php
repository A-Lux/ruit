<?php


namespace App\Http\Controllers;


use App\About;
use App\AboutAdvantage;
use App\AboutBanner;

class AboutController extends Controller
{
    public function index(){

        $banner = AboutBanner::getContent();
        $advantages = AboutAdvantage::getAll();
        $about = About::getContent();

        return view('about.index', compact('banner', 'advantages', 'about'));
    }
}
