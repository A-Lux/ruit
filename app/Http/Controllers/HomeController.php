<?php


namespace App\Http\Controllers;


use App\Certificate;
use App\MainAbout;
use App\MainAdvantage;
use App\MainBanner;

class HomeController extends Controller
{
    public function index()
    {

        $banner = MainBanner::getContent();
        $about = MainAbout::getContent();
        $advantages = MainAdvantage::getAll();
        $certificates = Certificate::getMain();

        return view('home.index', compact('banner', 'about', 'advantages', 'certificates'));
    }
}
