<?php


namespace App\Http\Controllers;


use App\Certificate;

class CertificateController extends Controller
{
    public function index(){

        $certificates = Certificate::getAll();

        return view('certificates.index', compact('certificates'));
    }
}
