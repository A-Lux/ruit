<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainAbout extends Model
{
    public static function getContent(){
        return self::select('content')->first();
    }
}
