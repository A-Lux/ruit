<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{
    public static function getContent(){
        return self::select('title', 'address', 'email', 'phone', 'map_url','phone_almaty')->first();
    }
}
