$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


function addError(input, span, error_text){
    span.innerHTML = error_text;
    span.style.display = 'block';
    input.addClass('border-error');
}

function removeError(input, span){
    span.innerHTML = '';
    span.style.display = 'none';
    input.removeClass('border-error');
}



$('body').on('click','.sbm_btn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/feedback',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {

            let name_input = $('[name=name]');
            let name_error = document.getElementById('name_error');
            let email_input = $('[name=email]');
            let email_error = document.getElementById('email_error');
            let phone_input = $('[name=phone]');
            let phone_error = document.getElementById('phone_error');
            let message = $('[name=message]');
            let message_error = document.getElementById('message_error');

            Swal.close();

            if(response.status == 1){

                removeError(name_input, name_error);
                removeError(email_input, email_error);
                removeError(phone_input, phone_error);
                removeError(message, message_error);

                $(".contacts_form_right form")[0].reset();
                Swal.fire(response.title, response.content, 'success');
            }else{

                if(response.errors['name'] != null) addError(name_input, name_error, response.errors['name']);
                else removeError(name_input, name_error);

                if(response.errors['email'] != null)addError(email_input, email_error, response.errors['email']);
                else removeError(email_input, email_error);

                if(response.errors['phone'] != null) addError(phone_input, phone_error, response.errors['phone']);
                else removeError(phone_input, phone_error);

                if(response.errors['message'] != null) addError(message, message_error, response.errors['message']);
                else removeError(message, message_error);
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Внутренняя ошибка сервера!'
            });
        }
    });
});
