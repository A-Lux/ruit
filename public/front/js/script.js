/*---------------------------- start Hamburger_menu_mobile ------------------------*/

$(document).ready(function(){
	$('.hamburger').click(function(){
		$(this).toggleClass('open');
		$(".navig").slideToggle( "slow" );
	});
});

/*---------------------------- finish Hamburger_menu_mobile ------------------------*/

/*---------------------------- start Slick_slider ------------------------*/

  $('.slider_sertificat').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  centerMode: true,
	  focusOnSelect: true,
	  centerPadding: '1px',
	  autoplaySpeed: 5000,
	  arrows: true,
	  prevArrow: $('.prev'),
	  nextArrow: $('.next'),
	  dots:true,
	  responsive: [
	    {
	      breakpoint: 1224,
	      settings: {
	        slidesToShow: 3
	      }
	    },
	    {
	      breakpoint: 750,
	      settings: {
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 460,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
	});
  $('.slider_sertificat_index').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  autoplay: true,
	  infinite: true,
	  centerMode: false,
	  centerPadding: '1px',
	  autoplaySpeed: 5000,
	  arrows: true,
	  prevArrow: $('.prev'),
	  nextArrow: $('.next'),
	  dots:false,
	  responsive: [
	    {
	      breakpoint: 1224,
	      settings: {
	        slidesToShow: 3
	      }
	    },
	    {
	      breakpoint: 930,
	      settings: {
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 460,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
	});

/*---------------------------- finish Slick_slider ------------------------*/
$(document).ready(function() {
	$('.popupe').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		
	});
});
