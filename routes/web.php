<?php

use Illuminate\Support\Facades\Route;
use App\Mail\NewRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/mail', function (){
    return new NewRequest();
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');

Route::get('/catalogs', 'ProductController@index')->name('products');
Route::get('/catalogs/{url}', 'ProductController@view')->name('product');

Route::get('/certificates', 'CertificateController@index')->name('certificates');
Route::get('/contacts', 'ContactController@index')->name('contacts');


Route::post('/request/feedback', 'RequestController@feedback')->name('feedback');
